"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from tokenize import String
from django.urls import path, include
from django.contrib import admin
from django.http import HttpResponse
import lab_2.urls as lab_2
import lab_3.urls as lab_3
from django.conf.urls.static import static
from . import settings
import lab_4.urls as lab_4

import lab_5.urls as lab_5
import lab_6.urls as lab_6

# method view
def index(request):
	return HttpResponse("Hello world!")

def welcome(request,name):
	return HttpResponse("Welcome, " + name + "!")

def hibro(request):
	return HttpResponse("Hibro!")

def about_me(request,name,hobby):
	return HttpResponse("Hibro! nama aku "+name +" hobby aku "+hobby)

def perhitungan(request,angka,angka2):
    
    return HttpResponse("Hasil perhitungan adalah " + str((angka + angka2)))

# TODO Create a new path that redirects to the aboutme page like welcome page (Show your name and your Hobby)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('welcome/<str:name>', welcome, name='welcome'),
    path('bro',hibro),
    path('aboutme/<str:name>/<str:hobby>', about_me ),
    path('lab-2/', include(lab_2)),
    path('lab-3/', include(lab_3)),
    path('lab-4/', include(lab_4)),
    path('lab-5/', include(lab_5)),
    path('lab-6/', include(lab_6)),
    path('api/', include('api.urls')),
    path('cobadjango/', include('cobadjango.urls')),
     path('cobaflut/', include('cobaflut.urls')),
    path('<int:angka>/+/<int:angka2>', perhitungan )
    # TODO Add a new path that redirects to the aboutme page 

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
