# Generated by Django 4.0.2 on 2022-03-02 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='trackertugas',
            name='detail',
            field=models.CharField(default='something', max_length=150),
        ),
    ]
