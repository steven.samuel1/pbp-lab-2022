from django.urls import include, path
from .views import index, list_tugas
import lab_2.urls as lab_2
from django.http import HttpResponse


urlpatterns = [
    path('', index, name='index'),
    path('tugas', list_tugas)
    # TODO Add 'tugas' path using list_tugas Views
]
