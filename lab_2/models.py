from datetime import date
from django.db import models

# TODO Create TrackerTugas model that contains course, detail, and deadline (date) here


class TrackerTugas(models.Model):
    course = models.CharField(max_length=30)
    detail = models.CharField(max_length=150,default = "something")
    deadline = models.DateField(default=date.today);
    
    # TODO Implement missing attributes in TrackerTugas model
