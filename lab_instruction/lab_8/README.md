# Lab 8: Flutter Input & Form

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Even Semester 2021/2022

---

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mampu memahami pemanfaatan Input & Form menggunakan flutter untuk membentuk struktur tampilan aplikasi mobile
- Mampu mengimplementasikan aplikasi mobile sederhana di atas framework flutter

## Tugas

Anda diminta untuk membuat sebuah app baru di dalam project ini bernama `lab_8` yang merupakan sebuah project di atas framework flutter yang menampilkan sebuah halaman yang sebelumnya sudah dibuat untuk platform web. Pastikan di dalam tersebut sudah mengandung:

1. Widget yang menampilkan Input Form element, cth: TextFormField, IconButton
2. Penerapan `onChanged` atau `onClick` untuk melakukan sebuah aksi sederhana, cth: menampilkan isi text, atau cetak input di layar console, dll

## Checklist

### Running Sample Program

1. [ ] Go to directory `lab_flutter`in root directory (`pbp-lab`).
2. [ ] Run `flutter create lab_8` to initiate flutter your `lab_8` directory. You can use template from `lab_instruction/lab_8/template_flutter`. And run `flutter run` to run your app in your `lab_flutter/lab_8` directory.

### Implement my own Input page

1. [ ] Try to reuse components and widget from boilerplate and modify it as required.
2. [ ] Try the application that you have built in this lab using Web Browser / Mobile Devices / Simulator, just run `flutter run`.

## Referensi

1. <https://belajarflutter.com/tutorial-cara-membuat-form-di-flutter-lengkap/>
2. Official Flutter Docs: <https://flutter.io/docs/>
3. [PBP Ganjil Lab-7](https://gitlab.com/PBP-2021/pbp-lab/-/blob/master/lab_instruction/lab_7/README.md)
4. Flutter Samples: <https://flutter.github.io/samples/#>
