from re import I
from django.urls import path
from .views import index, watch_list,add_watchlist


urlpatterns = [
    path('', index, name='index'),
     path('watchlist/', watch_list),
     path('form/',add_watchlist)
 
]
 