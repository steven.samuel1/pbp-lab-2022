from django.shortcuts import render
from django.http.response import HttpResponse
from lab_5.models import MyWatchlist
from .forms import TugasWatchlist
from django.contrib.auth.decorators import login_required

def index(request):
    watch  = MyWatchlist.objects.all()
    response = {'index': watch}
    return render(request, 'lab5_index.html', response)

def watch_list(request):
    watch  = MyWatchlist.objects.all()
    response = {'index': watch}
    return render(request, 'lab5_watchlist.html', response)


@login_required(login_url='/admin/login/')
def add_watchlist(request):
    form = TugasWatchlist(request.POST , request.FILES)
    # check if form data is valid
    if form.is_valid() and request.method == 'POST':
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-5/watchlist') 
    response = {'form':
    form}
    return render(request, "lab5_forms.html", response)