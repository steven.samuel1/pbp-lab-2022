from tkinter import Widget
from django import forms

from .models import MyWatchlist


class DateInput(forms.DateInput):
    input_type = 'date'



class TugasWatchlist(forms.ModelForm):
    class Meta:
        model = MyWatchlist
        fields = ['watched','title', 'rating', 'release_date', 'review', 'image_content']
        widgets = {
               
                'title' :  forms.TextInput(attrs={"title" : "input title"}),
                'rating' :  forms.TextInput(attrs={"form" : "inputform"}),
                'release_date' : DateInput(),
                'review' : forms.Textarea(attrs={"rows": 2}),
                'image_content' : forms.FileInput(attrs={"PHOTO" : "masukkin foto sini"}),
        }



