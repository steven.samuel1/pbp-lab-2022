from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_3.models import MywatchList;

# Create your views here.

def index(request):
    nonton = MywatchList.objects.all()  # TODO Implement this
    response = {'nonton': nonton}
    return render(request, 'lab3.html', response)

def xml(data):
    data = serializers.serialize('xml', MywatchList.objects.all())  # TODO Implement this
    return HttpResponse(data, content_type="application/xml")

def json(data):
    data = serializers.serialize('json', MywatchList.objects.all())  # TODO Implement this
    return HttpResponse(data, content_type="application/json")