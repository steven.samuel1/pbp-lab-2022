from django.urls import include, path
from .views import index, MywatchList
import lab_3.urls as lab_3
from django.http import HttpResponse
from .views import xml
from .views import json

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json)

    # TODO Add 'tugas' path using list_tugas Views
]