from django.db import models

# Create your models here.
class MywatchList(models.Model):
   watched = models.BooleanField(default=False)
   rating = models.FloatField(default=0.0)
   title = models.CharField(max_length=200,default="")
   release_Date = models.DateField(default="")
   review = models.CharField(max_length=100)
    