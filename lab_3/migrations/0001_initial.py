# Generated by Django 4.0.2 on 2022-03-20 01:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MywatchList',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('watched', models.BooleanField(default=False)),
                ('rating', models.FloatField(default=0.0)),
                ('title', models.CharField(default='', max_length=200)),
                ('release_Datea', models.DateField(default='')),
                ('review', models.CharField(max_length=100)),
            ],
        ),
    ]
