# Generated by Django 4.0.2 on 2022-03-20 02:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lab_3', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mywatchlist',
            old_name='release_Datea',
            new_name='release_Date',
        ),
    ]
