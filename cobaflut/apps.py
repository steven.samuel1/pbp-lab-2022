from django.apps import AppConfig


class CobaflutConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cobaflut'
