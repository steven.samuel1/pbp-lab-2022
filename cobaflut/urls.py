from django.urls import path
from django.urls.resolvers import URLPattern
from .views import getDataArticleAPI,daftarDataArticleAPI,json
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('add-produk/data-api', daftarDataArticleAPI, name='daftar'),
    path('add-produk/data-produk/<waktu>',getDataArticleAPI,name='data'),
    path('add-produk/json',json,name='json')
] + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
