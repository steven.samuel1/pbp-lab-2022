from http.server import HTTPServer
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from .models import Post
from django.views.decorators.csrf import csrf_exempt
import json as j
from django.core import serializers
from django.http.response import HttpResponse
# Create your views here.



@csrf_exempt
def getDataArticleAPI(request,waktu):
    if (request.method =='POST'):
        objek = Post.objects.filter(time=waktu)
        if (objek):
            dct = {
                'URLimageProduk' : (objek.values_list('URLimageProduk')[0][0]),
                'titleProduk' : (objek.values_list('titleProduk')[0][0]),
                'DeskripsiProduk' : (objek.values_list('DeskripsiProduk')[0][0]),
                'priceProduk' : (objek.values_list('priceProduk')[0][0]),
                'isiBagian' : (objek.values_list('isiBagian')[0][0]),
            }
            return JsonResponse(dct)
        else:
            dct = {'error'}
            return JsonResponse(dct,status=404)

@csrf_exempt
def daftarDataArticleAPI(request):
 if (request.method =='POST'):
    data = j.loads(request.body)
    URLimageProduk = data['URLimageProduk']
    titleProduk  = data['titleProduk']
    DeskripsiProduk = data['DeskripsiProduk']
    priceProduk = data['priceProduk']
    isiBagian = data['isiBagian']
    
    objek = Post(URLimageProduk=URLimageProduk,
         titleProduk=titleProduk,
         DeskripsiProduk=DeskripsiProduk,
         priceProduk=priceProduk,
         isiBagian=isiBagian)
    objek.save()
    return HttpResponse(objek,status=201)




def json(request):
    data = serializers.serialize('json',Post.objects.all())
    return HttpResponse(data, content_type="application/json")

