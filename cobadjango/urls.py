from django.urls import path
from cobadjango import views

urlpatterns = [
    path('student/', views.StudentList.as_view()),
]