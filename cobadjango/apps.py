from django.apps import AppConfig


class CobadjangoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cobadjango'
