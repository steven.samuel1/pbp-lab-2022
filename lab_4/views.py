from django.http import HttpResponse
from django.shortcuts import render
from lab_2.models import TrackerTugas
from .forms import TugasForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
   tugas_all = TrackerTugas.objects.all()
   response = { 'list_tugas' : tugas_all 
   }
   return render(request,'list_tugas.html',response)

@login_required(login_url="/admin/login/")
def tambah_tugas(request):
    tugas_form = TugasForm(request.POST or None)
    if request.method == 'POST' :
            if tugas_form.is_valid():
                tugas_form.save()
    nama_saya = "Steven"
    response = {
        'tugas_form': tugas_form,
        'nama' : nama_saya
    } 
    return render(request,'lab4_form.html',response)