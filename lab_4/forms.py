from tkinter import Widget
from django import forms
from lab_2.models import TrackerTugas


class DateInput(forms.DateInput):
    input_type = 'date'

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = "__all__"
        widgets = {
            'deadline': DateInput(),
            'course': forms.TextInput(attrs={"placeholder" : "masukkin sini"}),
            "detail" : forms.Textarea(attrs={"rows": 2}),
        }


